
PATH=/usr/local/bin:${PATH}:/usr/local/mysql/bin

if [ -f ~/bin/z/z.sh ]; then
  . ~/bin/z/z.sh
fi

export PS1="\u@mac \t \w \$ "

# Enable git-aware prompt (see https://github.com/jimeh/git-aware-prompt)
export GITAWAREPROMPT=~/.bash/git-aware-prompt
source $GITAWAREPROMPT/main.sh
#export PS1="\u@mac \t \w \[$txtcyn\]\$git_branch\[$txtred\]\$git_dirty\[$txtrst\]\$ "
export PS1="\u@mac \t \W \[$txtcyn\]\$git_branch\[$txtred\]\$git_dirty\[$txtrst\]\$ "
export SUDO_PS1="\[$bakred\]\u@\h\[$txtrst\] \w\$ "

export EDITOR='vim'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

alias ta='tmux -2 a'

alias vlc='/Applications/VLC.app/Contents/MacOS/VLC'

#alias db='mysql -uroot phoenix'
#alias db='mysql -uphx -pphx -h localhost -P 33060 --protocol=TCP d6phx'
alias db='mysql -h192.168.33.50 -uphx -pphx d6phx'
alias dbd='mysqldump -h192.168.33.50 -uphx -pphx d6phx '

alias bell='tput bel'
alias beep='tput bel'

alias wa='~/weball.sh'

alias gf='cd ~/Dropbox/gif; pwd;'
alias vm='cd ~/Dropbox/Voicemundo; pwd;'
alias gb='cd ~/Dropbox/GIFBOX_v2; pwd;'
alias cus='cd ~/phoenix-server/d6/sites/all/modules/custom; pwd;'
alias p='cd ~/phoenix-server/d6/; git branch; pwd;'
alias d6='cd ~/phoenix-server/d6/; git status; pwd;'
alias d66='cd ~/phoenix-server/d6/sites/all/modules/custom/; git status; pwd;'
alias d7='cd ~/phoenix-server/d7/sites/all/modules/custom/atd_backbone/modules/atd_order_edit/; git status; pwd;'
alias d77='cd ~/phoenix-server/d7/; git status; pwd;'
alias cup='date; echo d6...; cd ~/phoenix-server/d6; ctags --langmap=php:.engine.inc.module.theme.install.php --php-kinds=cdfi --languages=php --recurse --links=no; date; echo d7...; cd ~/phoenix-server/d7/; ctags --langmap=php:.engine.inc.module.theme.install.php --php-kinds=cdfi --languages=php --recurse --links=no; date;'
alias mw='ssh -i ~/Dropbox/Manawa/Website/AWS/manawa-yoga.pem ec2-user@54.77.84.31'
alias mwl='cd ~/manawa-web/; pwd;'
alias mwv='cd /Library/WebServer/Documents/vagrant/manawa; pwd; ls -1;'
alias v='cd /Library/WebServer/Documents/vagrant/multi; pwd; ls -1;'
alias vs='vagrant ssh'
alias vst='vagrant status'
alias vsus='vagrant suspend'
alias vsd='vagrant ssh d6phx'
alias vsdb='vagrant ssh db'
alias vsv='vagrant ssh vagrant'
alias vr='vagrant reload'
alias vh='vagrant halt'
alias vhf='vagrant halt -f'
alias vd='vagrant destroy'
alias vu='vagrant up'
alias vu2='vagrant up d6phx db d7 nginx varnish'
alias vu3='vagrant up db d7 nginx varnish'
alias vu4='vagrant up db d7 nginx varnish; vagrant halt --force d6phx; vagrant up d6phx'
alias vs6='cd /Library/WebServer/Documents/vagrant/multi/; vagrant ssh d6phx'
alias vs7='cd /Library/WebServer/Documents/vagrant/multi/; vagrant ssh d7'
alias vsc='cd /Library/WebServer/Documents/vagrant/multi/; vagrant ssh cache'
alias vs7w='cd /Library/WebServer/Documents/vagrant/multi/; vagrant ssh d7 -c "cd /var/www/html; watch drush cc css-js;"'
alias vsdb='cd /Library/WebServer/Documents/vagrant/multi/; vagrant ssh db'
alias vsp='cd /Library/WebServer/Documents/vagrant/polish/; vagrant ssh'
alias vsn='cd /Library/WebServer/Documents/vagrant/multi/; vagrant ssh nginx'
alias vsv='cd /Library/WebServer/Documents/vagrant/multi/; vagrant ssh varnish'
alias vup='vagrant up db search d7 d6phx nginx varnish'
alias ne="vs nginx -c 'tail -f /var/log/nginx/error.log'"

alias j='python -m json.tool'
alias ml='cd ~/Dropbox/Web\ localhost/milano/; pwd;'
alias drush='php ~/drush/drush.php'
alias e='drush ev'
alias c='drush cc all; echo ^G'
alias ccc='drush cc css+js; echo '
alias cca='drush cc advagg; echo '
alias ccm='drush cc menu; echo ^G'
alias ccl='~/drush_clear_locale_cache.sh; echo ^G'
alias wd='drush watchdog-show'
alias rr='ssh rangi@rangirobinson.com'
alias pe='tail -40 /var/log/php_errors; date;'
alias te='tail -f /var/log/php_errors'
alias d='cat /tmp/debug'
alias dl='cat /tmp/debug | less'
alias dh='cat /tmp/debug | head -30'
alias ll='pwd; ls -ltrah;'
alias l='pwd; ls -ltrah;'
alias a='pwd; ls -ltrah;'
alias b='~/php_beautify.sh'
alias gotosleep='~/gotosleep.sh'
alias n='~/node_info.sh'
alias gt='~/get_table.sh'

alias w4='ssh -t -L9631:localhost:631 92.52.65.238'
alias a1='ssh -t 78.136.61.228'
alias c2='ssh -t atdtravel.com'
alias c3='ssh -t 5.79.2.244'
alias dsd='ssh -t dosomethingdifferent.com'
alias d3='ssh -t atddev3.local'
alias pr='ssh -t -p 10022 -L9999:localhost:3128 pr'
alias ry='ssh -t ryu68.dyndns.org'
alias db1='ssh -t rangi@5.79.56.34'
alias db1v='ssh -t rangi@db1v'
alias db2='ssh -t rangi@162.13.220.66'
alias dw='ssh -t dwc'
alias dw-old='ssh 192.168.111.190'
alias nfs1='ssh -t nfs'
alias lb1='ssh -t lb1'
alias lb2='ssh -t lb2'
alias 1='ssh web1 -t'
alias 2='ssh web2 -t'
alias 3='ssh web3 -t'
alias 4='ssh web4 -t'
alias 5='ssh web5 -t'
alias 6='ssh web6 -t'
alias 7='ssh web7 -t'
alias 8='ssh web8 -t'
alias 9='ssh web9 -t'
alias 10='ssh web10 -t'
alias 11='ssh web11 -t'
alias s1='ssh s1 -t'
alias s2='ssh s2 -t'
alias s3='ssh s3 -t'
alias cache1='ssh -t cache1'
alias cache2='ssh -t cache2'
alias elk='ssh -t elk'
alias elk-old='ssh -t elk-old'
alias bm='ssh -t bm'

#alias r='sudo route delete default 192.168.111.253; sudo route add default 192.168.111.254'
#alias r2='sudo route delete default 192.168.111.254; sudo route add default 192.168.111.253'

alias gs='git status'
alias gc='git commit'
alias gd='git diff -w'
alias gl='git log'
alias gpl='git pull origin live'
alias gpm='git pull origin master'

alias vp='vim ~/.profile; source ~/.profile;'
alias vv='vim ~/.vimrc;'
alias vt='vim ~/.tmux.conf;'
alias cdv='cd ~/.vim; git status;'

alias h='vim ~/Dropbox/GTD/@HOME.txt'
alias gtd='cd ~/Dropbox/GTD/; pwd;'

alias revo='ssh revo.dyndns.org'

function g   { cd /Users/rangi/phoenix-server/d6/sites/all; grep -Ri "$1" modules/custom/ themes/ | grep -v fire/stylesheets | grep -v 'OTD Markup' | grep -v jquery | grep -v min.js | grep -v pika.js | grep -v ss-standard.js; }
function gtm { cd /Users/rangi/phoenix-server/d6/sites/all; grep -Ri "$1" themes/; }
function gm  { cd /Users/rangi/phoenix-server/d6/sites/all; grep -Ri "$1" modules/custom/ | grep -v fire/stylesheets; }
function ga  { cd /Users/rangi/phoenix-server/d6/sites/all; grep -Ri "$1" modules/ themes/ | grep -v fire/stylesheets; }
function gal { cd /Users/rangi/phoenix-server/d6/sites/all; grep -Rli "$1" modules/ themes/ | grep -v fire/stylesheets; }

find_function_custom () { grep -R "n $1" /Users/rangi/phoenix-server/d6/sites/all/modules/custom | cut -c 50-; }
alias ff=find_function_custom

find_function_contrib () { grep -R "n $1" /Users/rangi/phoenix-server/d6/sites/all/modules/contrib | cut -c 50-; }
alias fff=find_function_contrib

find_function_all () { grep -R "n $1" /Users/rangi/phoenix-server/d6/ | cut -c 31-; }
alias ffff=find_function_all


##
# Your previous /Users/rangi/.profile file was backed up as /Users/rangi/.profile.macports-saved_2012-04-02_at_23:35:11
##

# MacPorts Installer addition on 2012-04-02_at_23:35:11: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


# Voicemundo

# Add phrase via API and request recording:
function vma {
  curl -D - -d '{"phrase":"'"$1"'", "record":1}' http://www.rangirobinson.com/recordings/api/phrase
  echo
}


# Setting PATH for Python 3.5
# The orginal version is saved in .profile.pysave
#PATH="/Library/Frameworks/Python.framework/Versions/3.5/bin:${PATH}"
#export PATH
