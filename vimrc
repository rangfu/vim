" Tell vim to remember certain things when we exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo

set encoding=utf-8

" https://github.com/tpope/vim-pathogen
execute pathogen#infect()
" Add/update plugins' help text to vim's help system:
:Helptags

" Set up leader key
let mapleader = "\<space>"
set timeoutlen=500
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>z :wq<CR>
nnoremap <Leader>e :Explore<CR>
nnoremap <Leader>f :ALEFix eslint<CR>
nnoremap <Leader>0 f)i)<Esc>
nnoremap <Leader>; f;i)<Esc>
nnoremap <Leader>- :split<CR>
nnoremap <Leader>\ :vsplit<CR>
nnoremap <Leader>vv :edit ~/.vim/vimrc<CR>
nnoremap <Leader>vs :source ~/.vim/vimrc<CR>:echo "Reloaded .vimrc"<CR>
nnoremap <Leader>c :Commentary<CR>
nnoremap <Leader>t :FixWhitespace<CR>
nnoremap <Leader>l :ls<CR>:b
nnoremap <Leader>gs :Gstatus<CR>
nnoremap <Leader>ga :Gwrite<CR>
nnoremap <Leader>gr :Gremove<CR>
nnoremap <Leader>gd :Gvdiff<CR>
nnoremap <Leader>gb :Gblame<CR>
nnoremap <Leader>gg :Merginal<CR>
nnoremap <Leader>gc :Gcommit<CR>
nnoremap <Leader>gla :GitLog<CR>
nnoremap <Leader>gll :GitLog #<CR>
nnoremap <Leader>gpl :Gpull origin live<CR>
nnoremap <Leader>gpm :Gpull origin master<CR>
nnoremap <Leader>gpul :Gpush origin live<CR>
nnoremap <Leader>gpum :Gpush origin master<CR>
nnoremap <Leader>gpushl :Gpush origin live<CR>
nnoremap <Leader>gpushm :Gpush origin master<CR>
nnoremap <Leader>b :bd<CR>
nnoremap <Leader>wo <C-w><C-o>
nnoremap <Leader>= <C-w>=
nnoremap <Leader>o o<Esc>k
nnoremap <Leader>O O<Esc>j
nnoremap <Leader>php :set syntax=php<CR>
"nnoremap <Leader>s ^C
nnoremap <Leader><F10> :BreakpointWindow<CR>
nnoremap <Leader>r :BreakpointRemove *<CR>
nnoremap <C-Up> :resize -5<CR>
nnoremap <C-Down> :resize +5<CR>
nnoremap <C-Right> :vertical resize +10<CR>
nnoremap <C-Left> :vertical resize -10<CR>
nmap <F8> :TagbarToggle<CR>
nnoremap <Leader>vb :BreakpointWindow<CR>
nnoremap <Leader>vt :VdebugTrace
nnoremap <Leader>vc :Breakpoint conditional $
nnoremap <Leader>md :cexpr system('phpmd ' . shellescape(expand('%')) . ' text unusedcode')<CR>:copen<CR>

" Update (deploy) Lambda relating to given path:
nnoremap <Leader>ul !~/Dropbox/AWS/ng4/One/aws/lambdaUpdate.sh %<CR>

nmap <Leader>a= :Tabularize /=<CR>
vmap <Leader>a= :Tabularize /=<CR>
nmap <Leader>a: :Tabularize /:\zs<CR>

" YouCompleteMe
nnoremap <leader>jd :YcmCompleter GoToDefinition<CR>
nnoremap <leader>jr :YcmCompleter GoToReferences<CR>
nnoremap <leader>gt :YcmCompleter GetType<CR>
nnoremap <leader>yr :YcmCompleter RefactorRename 
"nnoremap <leader>gd :YcmCompleter GetDoc<CR>

" When typing { then enter, add closing } and place cursor between them:
inoremap {<CR> {<CR>}<C-o>O


" Remap K from defalt 'view man page for word under cursor' to 'grep word under cursor':
nnoremap K :grep <cword> .<CR>

" Set up alternative ESC key:
imap jj <Esc>


" Close NERDTree after opening file:
let NERDTreeQuitOnOpen=1

" Easy window navigation http://bit.ly/1GxIhZk
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Resize splits with shift-arrow:
map <S-right> :vertical resize +10<CR>
map <S-left> :vertical resize -10<CR>
map <S-up> :resize -5<CR>
map <S-down> :resize +5<CR>

" Set up keys for cycling through buffers.
" Unmapping <C-m> because this is equivalent to <Enter>, so has some
" annoying side effects. See http://bit.ly/1NB3M7W for more info.
"nnoremap <C-m> :bnext<CR>
nnoremap <C-n> :bprev<CR>

" Hide buffers rather than closing them when going to new files, so you
" don't have to save before leaving the current file:
set hidden

" Ignore case if search pattern is all lowercase, case-sensitive otherwise:
set smartcase

" Show matching brackets:
set showmatch

" http://vim.wikia.com/wiki/Browsing_programs_with_tags
set tags=./tags;

set relativenumber
set number

syntax on

" Treat .twig files as html:
au BufReadPost *.twig set syntax=html

function! ResCur()
  if line("'\"") <= line("$")
    normal! g`"
    return 1
  endif
endfunction

augroup resCur
  autocmd!
  autocmd BufWinEnter * call ResCur()
augroup END

set expandtab
set shiftwidth=2
set softtabstop=2
set smartindent
set backspace=2

" Use ESC to close command-t listings
" https://github.com/wincent/Command-T
let g:CommandTCancelMap = ['<ESC>', '<C-c>']

" CtrlP settings
" https://github.com/kien/ctrlp.vim
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_working_path_mode = 'r'
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.gz

" Set swap directory to be different than working directory:
" (the tailing double slash tells vim to use full directories, to avoid collisions)
set directory=/tmp//

" Diffs should be displayed side by side:
set diffopt=vertical

" Use Solarized colorscheme:
let g:solarized_termtrans = 1
set background=dark
colorscheme solarized

" You Covplete Me settings:
let g:ycm_filetype_specific_completion_to_disable = { 'gitcommit': 1, 'php': 1 }

" https://vi.stackexchange.com/questions/5128/matchpairs-makes-vim-slow
let g:matchparen_timeout = 2
let g:matchparen_insert_timeout = 2

" Syntastic settings
set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_php_checkers=['php']
let g:syntastic_html_checkers=['']

" https://github.com/Quramy/tsuquyomi
let g:tsuquyomi_disable_quickfix = 1
let g:syntastic_typescript_checkers = ['tsuquyomi'] " You shouldn't use 'tsc' checker.

" 2015-12-05 Rangi - this doesn't seem to be working yet
" UltiSnip setings
" https://github.com/SirVer/ultisnips
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
"let g:UltiSnipsListSnippets="<c-l>"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Airline settings:
" Show airline status bar even if only one window is open:
set laststatus=2
" Show buffer filenames in header:
"let g:airline#extensions#tabline#enabled = 1
" Show just buffer filenames instead of full paths:
"let g:airline#extensions#tabline#fnamemod = ':t'
" Enable fugitive integration:
let g:airline#extensions#branch#enabled = 1
" Enable syntasic integration:
let g:airline#extensions#syntastic#enabled = 1
" Enable tagbar integration:
let g:airline#extensions#tagbar#enabled = 1

" When editing crontab, don't save backups, as cron complains about this.
" http://calebthompson.io/crontab-and-vim-sitting-in-a-tree/
autocmd filetype crontab setlocal nobackup nowritebackup

" Vdebug settings
" Map local paths to remote:
"


let g:vdebug_options = {}
let g:vdebug_options.path_maps = {"/var/www/phoenix": "/Users/rangirobinson/phoenix-server/d6", "/var/www/global": "/Users/rangirobinson/phoenix-server/global", "/var/www/dsd": "/Users/rangirobinson/phoenix-server/d7dsd"}
let g:vdebug_options.continuous_mode = 0
let g:vdebug_options.break_on_open = 0
let g:vdebug_options.watch_window_style = "compact"

let g:vdebug_features = { 'max_children': 256, 'max_depth': 8 }

let g:debuggerMaxDepth = 5



" Set the minimum number of lines to appear above and below the cursor:
set scrolloff=3


" Use the The Silver Searcher instead of grep.
" https://robots.thoughtbot.com/faster-grepping-in-vim
if executable('ag')
  " Use ag over grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0
endif


" In the quickfix window, <CR> is used to jump to the error under the
" cursor, so undefine the mapping there.
autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>

" Ignore case when searching:
set ic

" After selecting a file with netrw, set c-^ to go to lasted edited file, rather than back to netrw
let g:netrw_altfile=1

" Configure phpfolding plugin
map zp :EnablePHPFolds<Cr>
let g:DisableAutoPHPFolding = 1

" Disable phpcomplete's enhanced 'jump to definition' function,
" which can be slow:
let g:phpcomplete_enhance_jump_to_definition = 0

" Configure vim-phpqa plugin
"let g:phpqa_codesniffer_autorun = 0
"let g:phpqa_codecoverage_autorun = 0
"let g:phpqa_messdetector_ruleset = "~/.vim/phpmd.xml"

filetype plugin on
set omnifunc=syntaxComplete#Complete

" Avoids long pause when saving TypeScript files.
" https://github.com/Quramy/tsuquyomi/issues/82
let g:tsuquyomi_use_vimproc=1

let g:ale_php_phpcs_standard="Drupal"

" let g:ale_fixers = { 'javascript': ['eslint'] }




" nvim-coc settings
" From https://github.com/neoclide/coc.nvim

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> <leader>d :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Rangi commenting out these ones to not interfere with ctrl-b for scroll up:
  " Remap <C-f> and <C-b> for scroll float windows/popups.
  " Note coc#float#scroll works on neovim >= 0.4.0 or vim >= 8.2.0750
  " nnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  " nnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  " inoremap <nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  " inoremap <nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"

" NeoVim-only mapping for visual mode scroll
" Useful on signatureHelp after jump placeholder of snippet expansion
if has('nvim')
  vnoremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#nvim_scroll(1, 1) : "\<C-f>"
  vnoremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#nvim_scroll(0, 1) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>E  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>i  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

